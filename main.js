const Koa = require('koa');
const staticFiles = require('koa-static');
const path = require('path');
const app = new Koa();

app.use(staticFiles(path.join(__dirname)))
app.listen(80);

console.log('ok')